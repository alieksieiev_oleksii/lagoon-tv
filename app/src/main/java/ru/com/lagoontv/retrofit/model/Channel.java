package ru.com.lagoontv.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Channel {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("path")
    @Expose
    private String path;

    @SerializedName("path_dvr")
    @Expose
    private String path_dvr;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("tv_show_id")
    @Expose
    private String tv_show_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath_dvr() { return path_dvr; }

    public void setPath_dvr(String path_dvr){this.path_dvr = path_dvr;}

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTvShowId() {
        return tv_show_id;
    }

    public void setTvShowId(String tv_show_id) {
        this.tv_show_id = tv_show_id;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", path_dvr='" + path_dvr + '\'' +
                ", image='" + image + '\'' +
                ", tv_show_id='" + tv_show_id + '\'' +
                '}';
    }
}

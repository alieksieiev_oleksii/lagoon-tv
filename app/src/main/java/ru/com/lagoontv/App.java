package ru.com.lagoontv;

import android.app.Application;
import android.content.Context;

import org.acra.ACRA;
import org.acra.annotation.AcraMailSender;

@AcraMailSender(
        mailTo = "freelagoontv@gmail.com",
        reportAsFile = false
)
public class App extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        ACRA.init(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}

package ru.com.lagoontv.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Conf {

    @SerializedName("status")
    @Expose
    private String status;


    /*Toasts*/
    @SerializedName("toast_main")
    @Expose
    private String toast_main;

    @SerializedName("toast_ch_det")
    @Expose
    private String toast_ch_det;

    @SerializedName("toast_player")
    @Expose
    private String toast_player;

    public String getStatus() {
        return status;
    }

    public String getToast_main() {
        return toast_main;
    }

    public String getToast_ch_det() {
        return toast_ch_det;
    }

    public String getToast_player() {
        return toast_player;
    }

    public boolean isAdmob_main_banner() {
        return admob_main_banner;
    }

    public boolean isAdmob_main_interstitial() {
        return admob_main_interstitial;
    }

    public boolean isAdmob_ch_detail_banner() {
        return admob_ch_detail_banner;
    }

    public boolean isAdmob_ch_detail_interstitial() {
        return admob_ch_detail_interstitial;
    }

    public boolean isAdmob_player_interstitial() {
        return admob_player_interstitial;
    }

    public String getTv_program_path() {
        return tv_program_path;
    }

    /*Admob*/
    @SerializedName("admob_main_banner")
    @Expose
    private boolean admob_main_banner;

    @SerializedName("admob_main_interstitial")
    @Expose
    private boolean admob_main_interstitial;

    @SerializedName("admob_ch_detail_banner")
    @Expose
    private boolean admob_ch_detail_banner;

    @SerializedName("admob_ch_detail_interstitial")
    @Expose
    private boolean admob_ch_detail_interstitial;

    @SerializedName("admob_player_interstitial")
    @Expose
    private boolean admob_player_interstitial;


    @SerializedName("dialogs")
    @Expose
    ArrayList<Dialog> dialogs;

    @SerializedName("tv_program_path")
    @Expose
    private String tv_program_path;

    @SerializedName("channels")
    @Expose
    private ArrayList<Channel> channels;


    public String getTvProgramPath() {
        return tv_program_path;
    }

    public ArrayList<Dialog> getDialogs() {
        return dialogs;
    }

    public ArrayList<Channel> getChannels() {
        return channels;
    }


    @Override
    public String toString() {
        return "Conf{" +
                "status='" + status + '\'' +
                ", toast_main='" + toast_main + '\'' +
                ", dialogs=" + dialogs +
                ", tv_program_path='" + tv_program_path + '\'' +
                ", channels=" + channels +
                '}';
    }
}

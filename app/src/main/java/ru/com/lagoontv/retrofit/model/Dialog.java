package ru.com.lagoontv.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dialog {

    @SerializedName("show_dialog")
    @Expose
    private String show_dialog;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("link")
    @Expose
    private String link;

    public String getShow_dialog() {
        return show_dialog;
    }

    public void setShow_dialog(String show_dialog) {
        this.show_dialog = show_dialog;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "Dialog{" +
                "show_dialog='" + show_dialog + '\'' +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}

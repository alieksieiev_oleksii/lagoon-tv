package ru.com.lagoontv.retrofit;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.com.lagoontv.retrofit.model.Conf;
import ru.com.lagoontv.retrofit.model.CurShow;

public interface ConfGetterAPI {

    @GET("rvk_tv_conf.json")
    Call<Conf> getConf();

    @GET("cur_shows")
    Call<List<CurShow>> getCurShows();

    @GET("get_all_show_by_id")
    Call<Map<String, List<CurShow>>> getAllShowsById(@Query("id") String id);
}

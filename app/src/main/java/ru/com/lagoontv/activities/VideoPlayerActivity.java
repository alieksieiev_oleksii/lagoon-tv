package ru.com.lagoontv.activities;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.com.lagoontv.R;
import ru.com.lagoontv.retrofit.ConfGetterAPI;
import ru.com.lagoontv.retrofit.model.Conf;

public class VideoPlayerActivity extends Activity implements OnPreparedListener{

    private InterstitialAd mInterstitialAd;
    boolean isAdmobInt;
    private VideoView videoView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().getBooleanExtra("dvr", false)){
            setContentView(R.layout.activity_video_player_dvr);
        }else {
            setContentView(R.layout.activity_video_player);
        }
        videoView = (VideoView)findViewById(R.id.video_view);

        MobileAds.initialize(this, "ca-app-pub-9740777732110436~4597406781");
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-9740777732110436/9020577969");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        /*==========================================================================*/
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url_conf))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConfGetterAPI confGetterAPI = retrofit.create(ConfGetterAPI.class);
        Call<Conf> call = confGetterAPI.getConf();
        //Палучаем конфиг из http://mobdev.com.ua/confs/rvk_tv_conf.json
        call.enqueue(new Callback<Conf>() {
            @Override
            public void onResponse(Call<Conf> call, Response<Conf> response) {
                isAdmobInt = response.body().isAdmob_player_interstitial();
            }

            @Override
            public void onFailure(Call<Conf> call, Throwable t) {
                showToast("onFailure retrofit callback Main");
            }
        });
    }

    @Override
    public void onPrepared() {
        videoView.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.setOnPreparedListener(this);
        videoView.setVideoURI(Uri.parse(getIntent().getStringExtra("path")));
    }

    @Override
    protected void onStop() {
        super.onStop();
        videoView.stopPlayback();
    }

    @Override
    public void onBackPressed() {
        if (mInterstitialAd.isLoaded() && isAdmobInt) {
            mInterstitialAd.show();
        } else {
            showToast("The interstitial wasn't loaded yet.");
        }
        super.onBackPressed();
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    // Shows the system bars by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    public void showToast(String string){
        Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
    }
}

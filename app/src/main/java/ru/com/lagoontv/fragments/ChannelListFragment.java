/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.com.lagoontv.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.com.lagoontv.R;
import ru.com.lagoontv.activities.ChannelDetailActivity;
import ru.com.lagoontv.retrofit.ConfGetterAPI;
import ru.com.lagoontv.retrofit.model.Channel;
import ru.com.lagoontv.retrofit.model.Conf;
import ru.com.lagoontv.retrofit.model.CurShow;
import ru.com.lagoontv.retrofit.model.Dialog;

public class ChannelListFragment extends Fragment {

    private static ArrayList<Channel> channels = new ArrayList<>();
    private static List<CurShow> curShows = new ArrayList<>();
    private RecyclerView recyclerView;
    private static String TAG = "RVK";
    private static String tv_program_path = "";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_chanels_list, container, false);

        /*==========================================================================*/
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url_conf))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConfGetterAPI confGetterAPI = retrofit.create(ConfGetterAPI.class);
        Call<Conf> call = confGetterAPI.getConf();
        //Палучаем конфиг из http://mobdev.com.ua/confs/rvk_tv_conf.json
        call.enqueue(new Callback<Conf>() {
            @Override
            public void onResponse(Call<Conf> call, Response<Conf> response) {
                Log.d(TAG, "onResponse: Conf "+ response.toString());
                //Log.d(TAG, "onResponse: received information from Conf"+ response.body().toString());
                ArrayList<Dialog> dialogs = response.body().getDialogs();
                channels = response.body().getChannels();

                /*==========================================================================*/
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(response.body().getTvProgramPath())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                tv_program_path = response.body().getTvProgramPath();
                ConfGetterAPI curShowsGetterAPI = retrofit.create(ConfGetterAPI.class);
                Call<List<CurShow>> call2 = curShowsGetterAPI.getCurShows();
                //Получаем список телепередач на данный момент
                call2.enqueue(new Callback<List<CurShow>>() {
                    @Override
                    public void onResponse(Call<List<CurShow>> call, Response<List<CurShow>> response) {
                        curShows = response.body();
                        setupRecyclerView(recyclerView);
                    }

                    @Override
                    public void onFailure(Call<List<CurShow>> call, Throwable t) {
                        Log.e(TAG, "onFailure. Something wrong" + t.getMessage());
                        Toast.makeText(getContext(), "onFailure. Something wrong " + t.getMessage(), Toast.LENGTH_SHORT).show();
                        setupRecyclerView(recyclerView);
                    }
                });
            }

            @Override
            public void onFailure(Call<Conf> call, Throwable t) {
                Log.e(TAG, "onFailure. Something wrong " + t.getMessage());
            }
        });
        return recyclerView;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new SimpleStringRecyclerViewAdapter(getActivity(), channels));
    }

    public static class SimpleStringRecyclerViewAdapter extends RecyclerView.Adapter<SimpleStringRecyclerViewAdapter.ViewHolder> {

        private final TypedValue mTypedValue = new TypedValue();
        private int mBackground;
        private Context context;

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final ImageView avatar;
            public final TextView channel_name;
            public final TextView tv_show_name;
            public final ProgressBar tv_show_progress;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                avatar = view.findViewById(R.id.avatar);
                channel_name = view.findViewById(R.id.channel_name);
                tv_show_name = view.findViewById(R.id.tv_show_name);
                tv_show_progress = view.findViewById(R.id.tv_show_progress);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + channel_name.getText();
            }
        }

        public SimpleStringRecyclerViewAdapter(Context context, ArrayList<Channel> channels) {
            this.context = context;
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
            mBackground = mTypedValue.resourceId;
        }

        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chanel_item, parent, false);
            view.setBackgroundResource(mBackground);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
            holder.channel_name.setText(channels.get(position).getName());
            //Получаем id канала из списка каналов в конфиге
                String showIdFromChannel = channels.get(position).getTvShowId();

                //Перебераем канала в curShows что бы по id найти нужный канал
                for (CurShow curShow : curShows){
                    if (showIdFromChannel != null && curShow != null){
                        if (showIdFromChannel.equals(curShow.getTvShowId())){
                            holder.channel_name.setText(channels.get(position).getName());
                            holder.tv_show_name.setText(curShow.getNameShow());
                            holder.tv_show_progress.setProgress(curShow.getProgress());
                        }
                    }
                }

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, ChannelDetailActivity.class);
                    intent.putExtra("id", channels.get(position).getTvShowId());
                    intent.putExtra("name", channels.get(position).getName());
                    intent.putExtra("path", channels.get(position).getPath());
                    intent.putExtra("path_dvr", channels.get(position).getPath_dvr());
                    intent.putExtra("tv_program_path", tv_program_path);
                    intent.putExtra("image", channels.get(position).getImage());
                    context.startActivity(intent);
                }
            });

            RequestOptions options = new RequestOptions();
            Glide.with(holder.avatar.getContext())
                    .load(channels.get(position).getImage())
                    .apply(options.fitCenter())
                    .into(holder.avatar);
        }

        @Override
        public int getItemCount() {
            return channels.size();
        }
    }
}

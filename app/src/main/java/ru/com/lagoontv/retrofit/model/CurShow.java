package ru.com.lagoontv.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CurShow {

    @SerializedName("nameChannel")
    @Expose
    private String tv_show_id;

    @SerializedName("nameShow")
    @Expose
    private String nameShow;

    @SerializedName("startTime")
    @Expose
    private String startTime;

    @SerializedName("endTime")
    @Expose
    private String endTime;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("dayOfWeek")
    @Expose
    private String dayOfWeek;

    @SerializedName("progress")
    @Expose
    private int progress;

    public String getTvShowId() {
        return tv_show_id;
    }

    public String getNameShow() {
        return nameShow;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getDescription() {
        return description;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public int getProgress() {
        return progress;
    }

    @Override
    public String toString() {
        return "CurShow{" +
                "nameChannel='" + tv_show_id + '\'' +
                ", nameShow='" + nameShow + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", description='" + description + '\'' +
                ", dayOfWeek='" + dayOfWeek + '\'' +
                ", progress=" + progress +
                '}';
    }
}

/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.com.lagoontv.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.com.lagoontv.adapters.CustomExpandableListAdapter;
import ru.com.lagoontv.R;
import ru.com.lagoontv.retrofit.ConfGetterAPI;
import ru.com.lagoontv.retrofit.model.Conf;
import ru.com.lagoontv.retrofit.model.CurShow;


public class ChannelDetailActivity extends AppCompatActivity {

    private String TAG = "RVK";
    private String tv_show_id;
    private String name;
    private String path;
    private String path_dvr;
    private String tv_program_path;
    private String startTime;
    private String stopTime;
    private String image;

    private ExpandableListView expandableListView;
    private ExpandableListAdapter expandableListAdapter;
    private List<String> expandableListTitle;
    private LinkedHashMap<String, List<String>> allShows;
    private List<String> oneChannel;
    private FloatingActionButton floatingActionButton;

    private Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
    private SimpleDateFormat sdfTvProgram = new SimpleDateFormat("yyyyMMddHHmm");
    private Date dStartTime;
    private Date dStopTime;

    private AdView mAdView;
    private InterstitialAd mInterstitialAd;
    private boolean isAdmobBanner;
    private boolean isAdmobInt;

    private Retrofit retrofit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        MobileAds.initialize(this, "ca-app-pub-9740777732110436~4597406781");
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-9740777732110436/9020577969");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();

        tv_show_id = getIntent().getStringExtra("id");
        name = getIntent().getStringExtra("name");
        path = getIntent().getStringExtra("path");
        path_dvr = getIntent().getStringExtra("path_dvr");
        image = getIntent().getStringExtra("image");
        tv_program_path = getIntent().getStringExtra("tv_program_path");
        Log.d(TAG, path_dvr);
        Log.d(TAG, tv_show_id);


        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        floatingActionButton = (FloatingActionButton)findViewById(R.id.floating_action_button);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), VideoPlayerActivity.class)
                        .putExtra("dvr", false)
                        .putExtra("path", path));
            }
        });



        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(name);

        loadBackdrop(image);

        /*==========================================================================*/
        retrofit = new Retrofit.Builder()
                .baseUrl(tv_program_path)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConfGetterAPI curShowsGetterAPI = retrofit.create(ConfGetterAPI.class);
        Call<Map<String, List<CurShow>>> call3 = curShowsGetterAPI.getAllShowsById(tv_show_id);
        //Получаем map c телепередачей на всю неделю
        call3.enqueue(new Callback<Map<String, List<CurShow>>>() {
            @Override
            public void onResponse(Call<Map<String, List<CurShow>>> call, Response<Map<String, List<CurShow>>> response) {
                allShows = new LinkedHashMap<String, List<String>>();

                //понедельник
                oneChannel = new ArrayList<>();
                for (CurShow curShow : response.body().get("понедельник")) {
                    oneChannel.add(getGoodViewTime(curShow.getStartTime()) + " " + curShow.getNameShow());
                }
                allShows.put("понедельник", oneChannel);

                //вторник
                oneChannel = new ArrayList<>();
                for (CurShow curShow : response.body().get("вторник")) {
                    oneChannel.add(getGoodViewTime(curShow.getStartTime()) + " " + curShow.getNameShow());
                }
                allShows.put("вторник", oneChannel);

                //среда
                oneChannel = new ArrayList<>();
                for (CurShow curShow : response.body().get("среда")) {
                    oneChannel.add(getGoodViewTime(curShow.getStartTime()) + " " + curShow.getNameShow());
                }
                allShows.put("среда", oneChannel);

                //четверг
                oneChannel = new ArrayList<>();
                for (CurShow curShow : response.body().get("четверг")) {
                    oneChannel.add(getGoodViewTime(curShow.getStartTime()) + " " + curShow.getNameShow());
                }
                allShows.put("четверг", oneChannel);

                //пятница
                oneChannel = new ArrayList<>();
                for (CurShow curShow : response.body().get("пятница")) {
                    oneChannel.add(getGoodViewTime(curShow.getStartTime()) + " " + curShow.getNameShow());
                }
                allShows.put("пятница", oneChannel);

                //суббота
                oneChannel = new ArrayList<>();
                for (CurShow curShow : response.body().get("суббота")) {
                    oneChannel.add(getGoodViewTime(curShow.getStartTime()) + " " + curShow.getNameShow());
                }
                allShows.put("суббота", oneChannel);

                //воскресенье
                oneChannel = new ArrayList<>();
                for (CurShow curShow : response.body().get("воскресенье")) {
                    oneChannel.add(getGoodViewTime(curShow.getStartTime()) + " " + curShow.getNameShow());
                }
                allShows.put("воскресенье", oneChannel);


                expandableListTitle = new ArrayList<String>(allShows.keySet());
                expandableListAdapter = new CustomExpandableListAdapter(getApplicationContext(), expandableListTitle, allShows);
                expandableListView.setAdapter(expandableListAdapter);

                expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                    @Override
                    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                        switch (i){
                            case 0:{
                                startTime = response.body().get("понедельник").get(i1).getStartTime();
                                stopTime = response.body().get("понедельник").get(i1).getEndTime();
                                startVideoActivityByTimes(startTime, stopTime);
                                break;
                            }case 1:{
                                startTime = response.body().get("вторник").get(i1).getStartTime();
                                stopTime = response.body().get("вторник").get(i1).getEndTime();
                                startVideoActivityByTimes(startTime, stopTime);
                                break;
                            }case 2:{
                                startTime = response.body().get("среда").get(i1).getStartTime();
                                stopTime = response.body().get("среда").get(i1).getEndTime();
                                startVideoActivityByTimes(startTime, stopTime);
                                break;
                            }case 3:{
                                startTime = response.body().get("четверг").get(i1).getStartTime();
                                stopTime = response.body().get("четверг").get(i1).getEndTime();
                                startVideoActivityByTimes(startTime, stopTime);
                                break;
                            }case 4:{
                                startTime = response.body().get("пятница").get(i1).getStartTime();
                                stopTime = response.body().get("пятница").get(i1).getEndTime();
                                startVideoActivityByTimes(startTime, stopTime);
                                break;
                            }case 5:{
                                startTime = response.body().get("суббота").get(i1).getStartTime();
                                stopTime = response.body().get("суббота").get(i1).getEndTime();
                                startVideoActivityByTimes(startTime, stopTime);
                                break;
                            }case 6:{
                                startTime = response.body().get("воскресенье").get(i1).getStartTime();
                                stopTime = response.body().get("воскресенье").get(i1).getEndTime();
                                startVideoActivityByTimes(startTime, stopTime);
                                break;
                            }
                        }
                        Log.d(TAG, String.valueOf(i) + " " + String.valueOf(i1) + " " + String.valueOf(l));
                        return false;
                    }
                });
//                Log.d(TAG,  allShows.keySet().toString());
//                Log.d(TAG,  oneChannel.toString());
            }

            @Override
            public void onFailure(Call<Map<String, List<CurShow>>> call, Throwable t) {
                Log.d(TAG, "No Good ");
                Log.d(TAG, t.toString());
            }
        });


        /*==========================================================================*/
        retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.base_url_conf))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConfGetterAPI confGetterAPI = retrofit.create(ConfGetterAPI.class);
        Call<Conf> call = confGetterAPI.getConf();
        //Палучаем конфиг из http://mobdev.com.ua/confs/rvk_tv_conf.json
        call.enqueue(new Callback<Conf>() {
            @Override
            public void onResponse(Call<Conf> call, Response<Conf> response) {
                isAdmobBanner = response.body().isAdmob_ch_detail_banner();
                isAdmobInt = response.body().isAdmob_ch_detail_interstitial();
                if (isAdmobBanner){
                    mAdView.loadAd(adRequest);
                }
            }

            @Override
            public void onFailure(Call<Conf> call, Throwable t) {
                showToast("onFailure retrofit callback Main");
            }
        });
    }

    private void loadBackdrop(String path) {
        final ImageView imageView = findViewById(R.id.backdrop);
        Glide.with(this).load(image).into(imageView);
    }

    private String getGoodViewTime (String time){
        try {
            Date dShowStartTime = sdfTvProgram.parse(time);
            calendar.setTime(dShowStartTime);
            int hour_start = calendar.get(Calendar.HOUR_OF_DAY);
            int min_start = calendar.get(Calendar.MINUTE);
            String resuaultString  = "";
            if (hour_start == 0){
                resuaultString = "00";
            }else {
                resuaultString = String.valueOf(hour_start);
            }
            resuaultString += ":";
            if (min_start == 0){
                resuaultString += "00";
            }else if (min_start > 0 && min_start < 10){
                resuaultString += "0";
                resuaultString += min_start;
            }else {
                resuaultString += String.valueOf(min_start);
            }
            return resuaultString;
            //return String.valueOf(((hour_start == 0)?"00":hour_start) + ":" + ((min_start == 0)?"00":min_start));
        }catch (Exception e){
            return "error";
        }
    }

    public boolean isTvShowBefore(String date){
        try {
            Date checkDate = sdfTvProgram.parse(date);
            Log.d(TAG, "Moscow - " + getMoscowTime().toString() + " Check time " + checkDate.toString() + " " + String.valueOf(checkDate.before(getMoscowTime())));
            return checkDate.before(getMoscowTime());
        }catch (Exception e){
            Log.d(TAG, e.getMessage());
            return false;
        }
    }

    private Date getMoscowTime(){
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        long gmtTime = calendar.getTime().getTime();
        long timezoneAlteredTime = gmtTime + TimeZone.getTimeZone("Europe/Moscow").getRawOffset();
        Calendar calendarMoscow = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        calendarMoscow.setTimeInMillis(timezoneAlteredTime);
        return calendarMoscow.getTime();
    }

    public void startVideoActivityByTimes(String startTime, String stopTime){
        if (isTvShowBefore(stopTime)){
            try {
                sdfTvProgram.setTimeZone(TimeZone.getTimeZone("Europe/Moscow"));
                dStartTime = sdfTvProgram.parse(startTime);
                dStopTime = sdfTvProgram.parse(stopTime);
                long difference = dStopTime.getTime()/1000 - dStartTime.getTime()/1000;
                String link = path_dvr
                        +dStartTime.getTime()/1000
                        +"-"
                        +difference
                        +".m3u8";
                Log.d(TAG,link);
                startActivity(new Intent(getApplicationContext(), VideoPlayerActivity.class)
                        .putExtra("dvr", true)
                        .putExtra("path", link));
            }catch (Exception e){
                Log.d(TAG, "Exeption");
            }
        }else {
            Toast.makeText(getApplicationContext(), "Архив не найден", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        if (mInterstitialAd.isLoaded() && isAdmobInt) {
            mInterstitialAd.show();
        } else {
            showToast("The interstitial wasn't loaded yet.");
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }
        super.onBackPressed();
    }

    public void showToast(String string){
        Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
    }

}
